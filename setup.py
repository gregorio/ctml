from setuptools import setup

setup(
    name='ctml',
    packages=['assignment5'],
    version='0.1',
    description='Computational Techniques for Machine Learning course repository',
    url='https://git.disroot.org/gregorio/ctml',
    author='gregorio, jessica, alejandro',
    author_email='gregorio@disroot.org',
    py_modules=['assignment5'],
    long_description=open('README.md').read(),
    license='CC-BY-4.0',
    install_requires=['numpy','pandas','brminer','scikit-posthocs','Orange3','seaborn','matplotlib','scipy','sklearn','requests'],
    )