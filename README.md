# CTML

Computational Techniques for Machine Learning course repository

Authors: Gregorio, Jessica, Alejandro


Instructions to run the three experiments in three steps:

1- install repository with:

pip install git+https://git.disroot.org/gregorio/ctml.git#egg=CTML

2- import assignment 5 library with:

from assignment5 import assignment5

3- run the experiments with:

assignment5.run_experiments(n_datasets = 60, data_transformation = None)

assignment5.run_experiments(n_datasets = 60, data_transformation = 'MinMax')

assignment5.run_experiments(n_datasets = 60, data_transformation = 'Standard')


Note: The run_experiments function downloads the necessary datasets to run the experiments.